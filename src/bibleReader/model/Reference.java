package bibleReader.model;

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Evan Mulshine (provided the implementation)
 */
public class Reference implements Comparable<Reference> {

	private BookOfBible book;
	private int chapter;
	private int verse;

	/**
	 * Construct a Reference with the given book, chapter, and verse.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 */
	public Reference(BookOfBible book, int chapter, int verse) {
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	/**
	 * Returns the book of this Reference as a String.
	 * 
	 * @return A String representation of the book name
	 */
	public String getBook() {
		return book.toString();
	}

	/**
	 * Returns the book of this reference as a BookOfBible.
	 * 
	 * @return The BookOfBible of this Reference
	 */
	public BookOfBible getBookOfBible() {
		return book;
	}

	/**
	 * Returns the chapter number of this Reference.
	 * 
	 * @return The chapter number
	 */
	public int getChapter() {
		return chapter;
	}

	/**
	 * Returns the verse number of this reference.]
	 * 
	 * @return The verse number
	 */
	public int getVerse() {
		return verse;
	}

	/**
	 * Returns a String representation of the reference in the form: "book
	 * chapter:verse" For instance, "Genesis 2:3".
	 * 
	 * @return The String representation of this Reference
	 */
	@Override
	public String toString() {
		return book.toString() + " " + chapter + ":" + verse;
	}

	/**
	 * Checks if this reference is equal to the given object.
	 * 
	 * @param other The object to check equality with
	 * @return True if and only if the given object is a Reference of the same value
	 */
	@Override
	public boolean equals(Object other) {
		// Check for reference equality.
		if (this == other) {
			return true;
		}
		// Check if other is a Reference.
		if (!(other instanceof Reference)) {
			return false;
		}
		// Check content equality.
		Reference ref = (Reference) other;
		if (book.equals(ref.getBookOfBible()) && chapter == ref.getChapter() && verse == ref.getVerse()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return an integer representation of this object.
	 * 
	 * @return An integer representation of this Reference
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * Compares this Reference to the given Reference to see where in the Bible they
	 * are in relation to one another.
	 * 
	 * @param otherRef The Reference to compare to
	 * @return A negative number, 0, or a positive number if the given Reference is
	 *         before, equal to, or after this Reference.
	 */
	@Override
	public int compareTo(Reference otherRef) {
		int count = book.ordinal() * 1000000 + chapter * 1000 + verse;
		int otherCount = otherRef.getBookOfBible().ordinal() * 1000000 + otherRef.getChapter() * 1000
				+ otherRef.getVerse();
		return count - otherCount;
	}
}

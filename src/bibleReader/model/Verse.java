package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Evan Mulshine (provided the implementation)
 */
public class Verse implements Comparable<Verse> {

	private Reference reference;
	private String text;

	/**
	 * Construct a verse given the reference and the text.
	 * 
	 * @param r The reference for the verse
	 * @param t The text of the verse
	 */
	public Verse(Reference r, String t) {
		this.reference = r;
		this.text = t;
	}

	/**
	 * Construct a verse given the book, chapter, verse, and text.
	 * 
	 * @param book    The book of the Bible
	 * @param chapter The chapter number
	 * @param verse   The verse number
	 * @param text    The text of the verse
	 */
	public Verse(BookOfBible book, int chapter, int verse, String text) {
		this.reference = new Reference(book, chapter, verse);
		this.text = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		return reference;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 * 
	 * @return A String representation of this Verse
	 */
	@Override
	public String toString() {
		return reference + " " + text;
	}

	/**
	 * Return true if and only if they have the same reference and text.
	 * 
	 * @param other The object to check equality with
	 * @return True if and only if the given object is a Verse with the same
	 *         Reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		// Check for reference equality.
		if (this == other) {
			return true;
		}
		// Check if other is a Verse.
		if (!(other instanceof Verse)) {
			return false;
		}
		// Check content equality.
		Verse otherVerse = (Verse) other;
		Reference otherRef = otherVerse.getReference();
		String otherText = otherVerse.getText();
		if (reference.equals(otherRef) && text.equals(otherText)) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Return an integer representation of this Verse.
	 * 
	 * @return An integer representation of this Verse
	 */
	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	/**
	 * Compares this Verse to the given Verse to see where in the Bible they are in
	 * relation to one another. If they have the same reference, compare the Verses'
	 * texts lexicographically.
	 * 
	 * @param other The Verse to compare to
	 * @return A negative number, 0, or a positive number if the given Verse is
	 *         before, equal to, or after this Verse.
	 */
	@Override
	public int compareTo(Verse other) {
		// Compares References if they are not equal.
		if (!reference.equals(other.getReference())) {
			return reference.compareTo(other.getReference());
		}
		// Compares text if they are.
		else {
			return text.compareTo(other.getText());
		}
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return reference.equals(other.getReference());
	}

}

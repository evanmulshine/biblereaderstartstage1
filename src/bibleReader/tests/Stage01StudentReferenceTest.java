package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;

/*
 * Tests for the Reference class.
 * @author Evan Mulshine and Dre Solorzano
 */

// Make sure you test at least the following:
// --All of the constructors and all of the methods.
// --Using a full title or an abbreviation for a book name.
// --The equals for success and for failure, especially when they match in 2 of the 3 places
//    (e.g. same book and chapter but different verse, same chapter and verse but different book, etc.)
// --That compareTo is ordered properly for similar cases as the equals tests.
//    (e.g. should Genesis 1:1 compareTo Revelation 22:21 return a positive or negative number?)
// --Any other potentially tricky things.

public class Stage01StudentReferenceTest {

	// A few sample references to get you started.
	private Reference	gen1_1;
	private Reference	rev1_1;
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	@Before
	public void setUp() throws Exception {
		// A few sample references to get you started.
		gen1_1 = new Reference(BookOfBible.Genesis, 1, 1);
		rev1_1 = new Reference(BookOfBible.Revelation, 1, 1);
		
		// TODO Create more objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	@Test
	public void testGetters() {
		Reference gen1_2 = new Reference(BookOfBible.Genesis, 1, 2);
		assertEquals("Genesis", gen1_2.getBook());
		assertEquals(BookOfBible.Genesis, gen1_2.getBookOfBible());
		assertEquals(1, gen1_2.getChapter());
		assertEquals(2, gen1_2.getVerse());
		
		Reference john3_16 = new Reference(BookOfBible.John, 3, 16);
		assertEquals("John", john3_16.getBook());
		assertEquals(BookOfBible.John, john3_16.getBookOfBible());
		assertEquals(3, john3_16.getChapter());
		assertEquals(16, john3_16.getVerse());
		
		Reference rev50_100 = new Reference(BookOfBible.Revelation, 50, 100);
		assertEquals("Revelation", rev50_100.getBook());
		assertEquals(BookOfBible.Revelation, rev50_100.getBookOfBible());
		assertEquals(50, rev50_100.getChapter());
		assertEquals(100, rev50_100.getVerse());
	}
	
	@Test
	public void testToString() {
		Reference gen5_10 = new Reference(BookOfBible.Genesis, 5, 10);
		assertEquals("Genesis 5:10", gen5_10.toString());
		assertEquals("Revelation 1:1", rev1_1.toString());
	}
	
	@Test
	public void testEquals() {
		Reference ruth2_3 = new Reference(BookOfBible.Ruth, 2, 3);
		Reference ruth2_3a = new Reference(BookOfBible.Ruth, 2, 3);
		assertTrue(ruth2_3.equals(ruth2_3a));
		assertTrue(ruth2_3a.equals(ruth2_3));
		assertEquals(ruth2_3.hashCode(), ruth2_3a.hashCode());
		
		Reference ruth3_2 = new Reference(BookOfBible.Ruth, 3, 2);
		assertFalse(ruth2_3.equals(ruth3_2));
		assertFalse(ruth3_2.equals(ruth2_3));
		assertFalse(gen1_1.equals(ruth2_3));
		assertFalse(ruth2_3.hashCode() == ruth3_2.hashCode());
		
	}
	
	@Test
	public void testHashCode() {
		Reference rev1_2 = new Reference(BookOfBible.Revelation, 1, 2);
		Reference rev1_2a = new Reference(BookOfBible.Revelation, 1, 2);
		
		assertTrue(rev1_2.equals(rev1_2a));
		assertEquals(rev1_2.hashCode(), rev1_2a.hashCode());
		
		assertFalse(gen1_1.equals(rev1_2));
		assertFalse(gen1_1.hashCode() == rev1_2.hashCode());
	}

	@Test
	public void testcompareTo() {
		Reference gen1_2 = new Reference(BookOfBible.Genesis, 1, 2);
		Reference gen1_2a = new Reference(BookOfBible.Genesis, 1, 2);
		Reference gen2_1 = new Reference(BookOfBible.Genesis, 2, 1);
		Reference exo1_1 = new Reference(BookOfBible.Exodus, 1, 1);
		
		assertTrue(gen1_1.compareTo(gen1_2) < 0);
		assertTrue(gen1_1.compareTo(gen2_1) < 0);
		assertTrue(gen1_1.compareTo(exo1_1) < 0);
		
		assertTrue(gen1_2.compareTo(gen1_1) > 0);
		assertTrue(gen2_1.compareTo(gen1_1) > 0);
		assertTrue(exo1_1.compareTo(gen1_1) > 0);
		
		assertEquals(0, gen1_2.compareTo(gen1_2a));
		
	}

}

package bibleReader.tests;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import bibleReader.model.BookOfBible;
import bibleReader.model.Reference;
import bibleReader.model.Verse;


/*
 * Tests for the Verse class.
 * @author Evan Mulshine and Dre Solorzano
 */

public class Stage01StudentVerseTest {
	/*
	 * Anything that should be done before each test.
	 * For instance, you might create some objects here that are used by several of the tests.
	 */
	private Verse gen1_4;
	private Reference gen1_4ref;
	private Verse num20_34;
	private Reference num20_34ref;
	private Verse rev10_1;
	private Reference rev10_1ref;
	
	@Before
	public void setUp() throws Exception {
		// TODO Create objects that the tests will use here.
		// You need to make them fields so they can be seen in the test methods.
		gen1_4 = new Verse(BookOfBible.Genesis, 1, 4, "Genesis 1:4 text");
		gen1_4ref = new Reference(BookOfBible.Genesis, 1, 4);
		num20_34 = new Verse(BookOfBible.Numbers, 20, 34, "Numbers 20:34 text");
		num20_34ref = new Reference(BookOfBible.Numbers, 20, 34);
		rev10_1 = new Verse(BookOfBible.Revelation, 10, 1, "Revelation 10:1 text");
		rev10_1ref = new Reference(BookOfBible.Revelation, 10, 1);
	}

	/*
	 * Anything that should be done at the end of each test.  
	 * Most likely you won't need to do anything here.
	 */
	@After
	public void tearDown() throws Exception {
		// You probably won't need anything here.
	}

	/*
	 * Add as many test methods as you think are necessary. Two suggestions (without implementation) are given below.
	 */
	
	@Test
	public void testGetters() {
		assertEquals(gen1_4ref, gen1_4.getReference());
		assertEquals(num20_34ref, num20_34.getReference());
		assertEquals(rev10_1ref, rev10_1.getReference());
		
		assertEquals("Genesis 1:4 text", gen1_4.getText());
		assertEquals("Numbers 20:34 text", num20_34.getText());
		assertEquals("Revelation 10:1 text", rev10_1.getText());
	}
	
	@Test
	public void testToString() {
		assertEquals("Genesis 1:4 Genesis 1:4 text", gen1_4.toString());
		assertEquals("Numbers 20:34 Numbers 20:34 text", num20_34.toString());
		assertEquals("Revelation 10:1 Revelation 10:1 text", rev10_1.toString());
	}
	
	@Test
	public void testEquals() {
		Reference ezra6_3ref = new Reference(BookOfBible.Ezra, 6, 3);
		Verse ezra6_3a = new Verse(ezra6_3ref, "Ezra 6:3 text");
		Verse ezra6_3b = new Verse(BookOfBible.Ezra, 6, 3, "Ezra 6:3 text");
		assertTrue(ezra6_3a.equals(ezra6_3b));
		assertTrue(ezra6_3b.equals(ezra6_3a));
		assertEquals(ezra6_3a.hashCode(), ezra6_3b.hashCode());
		
		assertFalse(ezra6_3a.equals(rev10_1));
		assertFalse(rev10_1.equals(ezra6_3a));
		assertFalse(ezra6_3a.hashCode() == rev10_1.hashCode());
	}
	
	@Test
	public void testHashCode() {
		Verse gen1_4a = new Verse(gen1_4ref, "Genesis 1:4 text");
		assertTrue(gen1_4.equals(gen1_4a));
		assertEquals(gen1_4.hashCode(), gen1_4a.hashCode());
		
		assertFalse(gen1_4.equals(rev10_1));
		assertFalse(gen1_4.hashCode() == rev10_1.hashCode());
	}
	
	@Test
	public void testCompareTo() {
		Verse gen1_5 = new Verse(BookOfBible.Genesis, 1, 5, "Genesis 1:5 text");
		Verse gen2_1 = new Verse(BookOfBible.Genesis, 2, 1, "Genesis 2:1 text");
		Verse gen1_4a = new Verse(gen1_4ref, "Genesis 1:4 text");
		
		assertTrue(gen1_4.compareTo(gen1_5) < 0);
		assertTrue(gen1_4.compareTo(gen2_1) < 0);
		assertTrue(gen1_4.compareTo(num20_34) < 0);
		assertTrue(gen1_4.compareTo(rev10_1) < 0);
		
		assertTrue(gen1_5.compareTo(gen1_4) > 0);
		assertTrue(gen2_1.compareTo(gen1_5) > 0);
		assertTrue(num20_34.compareTo(gen2_1) > 0);
		assertTrue(rev10_1.compareTo(num20_34) > 0);
		
		assertEquals(0, gen1_4.compareTo(gen1_4a));
	}
	
	@Test
	public void testSameReference() {
		Verse num20_34a = new Verse(BookOfBible.Numbers, 20, 34, "Numbers 20:34 text");
		Verse rev10_1a = new Verse(BookOfBible.Revelation, 10, 1, "Revelation 10:1 text");
		
		assertTrue(num20_34.sameReference(num20_34a));
		assertTrue(num20_34a.sameReference(num20_34));
		
		assertTrue(rev10_1.sameReference(rev10_1a));
		assertTrue(rev10_1a.sameReference(rev10_1));
		
		assertFalse(num20_34.sameReference(rev10_1));
	}
}
